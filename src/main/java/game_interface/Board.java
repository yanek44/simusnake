package game_interface;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import snake.Game;

import java.net.URL;

public class Board extends StackPane {
    Node gamePanel;
    Game game;
    StartPanel startPanel;

    public Board() {
        super();
        URL clazz = this.getClass().getResource("start-panel.css");
        if (clazz != null)
            this.getStylesheets().add(clazz.toExternalForm());

        this.getStyleClass().add("backpane");
        startPanel = new StartPanel("SimuSnake !", "Jouer !");
        this.getChildren().add(startPanel);
        startPanel.timeToPlay.addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                onPlay();
            }
        });
    }

    private void onPlay() {
        getChildren().remove(startPanel);

        game = new Game(startPanel.getX(), startPanel.getY());
        game.lostProperty().addListener((obs, oldV, newV) -> onLost());
        game.prefHeightProperty().bind(Bindings.min(this.widthProperty(),this.heightProperty()));
        game.prefWidthProperty().bind(Bindings.min(this.widthProperty(),this.heightProperty()));
        HBox h = new HBox(game);
        h.setAlignment(Pos.CENTER);
        gamePanel = new VBox(h);
        ((VBox)gamePanel).setAlignment(Pos.CENTER);
        getChildren().add(gamePanel);
        Platform.runLater(game::requestFocus);
    }

    private void onLost() {
        this.getChildren().remove(gamePanel);
        startPanel = new StartPanel("Perdu !  Score : " + game.getScore(), "Rejouer");
        this.getChildren().add(startPanel);
        startPanel.timeToPlay.addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                onPlay();
            }
        });
    }

    public static Stage getStage(){
        Stage stage = new Stage();
        stage.setTitle("snake");

        Board snake= new Board();
        Scene scene = new Scene(snake);
        stage.setScene(scene);
        stage.setFullScreen(true);
        snake.setPrefSize(scene.getWidth(),scene.getHeight());
        scene.widthProperty().addListener((observable, oldValue, newValue) -> snake.setPrefWidth(newValue.doubleValue()));
        scene.heightProperty().addListener((observable, oldValue, newValue) -> snake.setPrefHeight(newValue.doubleValue()));
        return stage;
    }
}
