package game_interface;

import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.text.Font;

import java.net.URL;

public class StartPanel extends GridPane {
    final SimpleBooleanProperty timeToPlay;
    final TextField x;
    final TextField y;

    public StartPanel(String titreString, String playButton) {
        super();
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setPercentWidth(100d / 3d);
        this.getColumnConstraints().addAll(column1, column1, column1);
        RowConstraints row1 = new RowConstraints();
        row1.setPercentHeight(40);
        RowConstraints row3 = new RowConstraints();
        row3.setPercentHeight(20);
        this.getRowConstraints().addAll(row1, row1, row3);

        timeToPlay = new SimpleBooleanProperty(false);
        URL clazz = this.getClass().getClassLoader().getResource("game_interface/start-panel.css");
        if (clazz != null)
            this.getStylesheets().add(clazz.toExternalForm());

        Label titre = new Label(titreString);
        titre.getStyleClass().add("titre");
        GridPane.setHgrow(titre, Priority.ALWAYS);
        GridPane.setHalignment(titre, HPos.CENTER);

        x = new TextField("10");
        x.setPrefColumnCount(2);
        y = new TextField("12");
        y.setPrefColumnCount(2);
        Label fois = new Label("x");
        fois.setFont(new Font(50));
        HBox taille = new HBox(x, fois, y);
        taille.setFillHeight(true);
        taille.setAlignment(Pos.CENTER);

        Button jouer = new Button(playButton);
        jouer.setOnAction(event -> timeToPlay.set(true));
        GridPane.setHalignment(jouer, HPos.CENTER);
        GridPane.setValignment(jouer, VPos.CENTER);

        Button quitter = new Button("Quitter");
        quitter.setOnAction(event -> Platform.exit());
        GridPane.setHalignment(quitter, HPos.CENTER);
        GridPane.setValignment(quitter, VPos.CENTER);

        this.add(titre, 0, 0, 3, 1);
        this.add(taille, 0, 1, 3, 1);
        this.add(quitter, 0, 2);
        this.add(jouer, 2, 2);
    }

    public int getX() {
        return Integer.parseInt(x.getText());
    }

    public int getY() {
        return Integer.parseInt(y.getText());
    }
}
