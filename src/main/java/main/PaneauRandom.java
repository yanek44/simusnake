package main;

import game_interface.Board;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;

import static javafx.scene.input.KeyCode.*;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;


public class PaneauRandom extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    private static final KeyCode[] konamiCode = {UP, UP, DOWN, DOWN, LEFT, RIGHT, LEFT, RIGHT, B, A};

    private int avancementInCode = 0;

    public void start(Stage stage) {
        stage.setTitle("Random");

        Scene scene = new Scene(new Pane());
        stage.setScene(scene);
        stage.setWidth(200);
        stage.setHeight(200);
        stage.show();

        scene.addEventFilter(KeyEvent.KEY_RELEASED, event -> {
            if (event.getCode().equals(konamiCode[avancementInCode])) {
                avancementInCode++;
            }
            else{
                avancementInCode=0;
            }
            System.out.println(avancementInCode);
            if (avancementInCode == konamiCode.length-1) {
                Board.getStage().show();
            }
        });
    }
}
