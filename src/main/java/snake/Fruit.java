package snake;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

public class Fruit extends Rectangle {

    private static final Image voyageurImg;

    static {
        voyageurImg = new Image(Player.class.getResource("voy.png").toExternalForm());
    }

    final SimpleIntegerProperty posX;
    final SimpleIntegerProperty posY;

    final SimpleBooleanProperty eaten;

    public Fruit(){
        super();
        eaten = new SimpleBooleanProperty();
        posX = new SimpleIntegerProperty(0);
        posY = new SimpleIntegerProperty(0);

        this.setFill(new ImagePattern(voyageurImg));

        eaten.addListener((observable, oldValue, newValue) -> setVisible(!newValue));
        eaten.set(true);
    }

    public boolean isEaten() {
        return eaten.get();
    }
}
