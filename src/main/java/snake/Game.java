package snake;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.BooleanProperty;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.util.Duration;
import javafx.util.Pair;

import java.util.Random;

import static snake.PlayerDirection.*;

public class Game extends Pane {

    final Fruit fruit;
    final Player player;
    final int xSize;
    final int ySize;

    final Timeline playerTimeline;
    public Game(int xSize, int ySize) {
        this.setStyle("-fx-background-color: white;");
        fruit = new Fruit();
        player = new Player();
        this.xSize = xSize;
        this.ySize = ySize;

        this.prefHeightProperty().bindBidirectional(this.prefWidthProperty());

        this.getChildren().addAll(fruit, player);

        fruit.widthProperty().bind(this.widthProperty().divide(xSize));
        fruit.heightProperty().bind(this.heightProperty().divide(ySize));
        player.width.bind(this.widthProperty().divide(xSize));
        player.height.bind(this.heightProperty().divide(ySize));

        initItemPosBinding();
        initKeyListener();
        initPlayerHittingWall();

        //Boucle de jeu
        playerTimeline = new Timeline(new KeyFrame(Duration.seconds(1), event -> playerTimeLineAction()));
        playerTimeline.setCycleCount(Animation.INDEFINITE);
        playerTimeline.play();

        Timeline fruitTimeline = new Timeline(new KeyFrame(Duration.seconds(1), event -> fruitTimeLineAction()));
        fruitTimeline.setCycleCount(Animation.INDEFINITE);
        fruitTimeline.play();
    }

    private void fruitTimeLineAction() {
        if(fruit.isEaten()){
            Random r = new Random();
            int newXPos = r.nextInt(xSize);
            int newYPos = r.nextInt(ySize);
            while(player.getAllPosition().contains(new Pair<>(newXPos,newYPos))){
                newXPos = r.nextInt(xSize);
                newYPos = r.nextInt(ySize);
            }
            fruit.posX.set(newXPos);
            fruit.posY.set(newYPos);
            fruit.eaten.set(false);
        }
    }

    private void playerTimeLineAction() {
        //Si le joueur est au meme endroit que le fruit
        if(player.position.get().posX == fruit.posX.get() && player.position.get().posY == fruit.posY.get()) {
            fruit.eaten.set(true);
            player.addTailSize();
            playerTimeline.setRate(playerTimeline.getRate()*Math.log10(getScore()+10));
        }
        if (player.lastDirectionInput != null) {
            switch (player.lastDirectionInput) {
                case DROITE:
                    player.setPosX(player.position.get().posX + 1);
                    break;
                case GAUCHE:
                    player.setPosX(player.position.get().posX - 1);
                    break;
                case HAUT:
                    player.setPosY(player.position.get().posY - 1);
                    break;
                case BAS:
                    player.setPosY(player.position.get().posY + 1);
                    break;
            }
        }
    }

    private void initKeyListener() {
        this.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.UP)) {
                player.lastDirectionInput = HAUT;
            }
            if (event.getCode().equals(KeyCode.DOWN)) {
                player.lastDirectionInput = BAS;
            }
            if (event.getCode().equals(KeyCode.LEFT)) {
                player.lastDirectionInput = GAUCHE;
            }
            if (event.getCode().equals(KeyCode.RIGHT)) {
                player.lastDirectionInput = DROITE;
            }
        });
    }

    private void initItemPosBinding() {
        fruit.translateXProperty().bind(this.widthProperty().divide(xSize).multiply(fruit.posX));
        fruit.translateYProperty().bind(this.heightProperty().divide(ySize).multiply(fruit.posY));

        player.position.set(new Player.Position(xSize/2,ySize/2));
    }

    private void initPlayerHittingWall() {
        player.position.addListener((observable, oldValue, newValue) -> {
            if (newValue.posX < 0 || newValue.posX >= xSize || newValue.posY < 0 || newValue.posY >= ySize) {
                player.lost.setValue(true);
                player.lastDirectionInput = null;
            }
        });
    }

    public BooleanProperty lostProperty() {
        return player.lost;
    }
    public int getScore(){return player.getChildrenUnmodifiable().size();}
}
