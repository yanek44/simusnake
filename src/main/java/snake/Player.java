package snake;

import javafx.beans.property.*;
import javafx.scene.Parent;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.util.Pair;

import java.util.Collection;
import java.util.stream.Collectors;

public class Player extends Parent {

    PlayerDirection lastDirectionInput;
    PlayerPart head;
    final SimpleObjectProperty<Position> position;
    final SimpleDoubleProperty width;
    final SimpleDoubleProperty height;
    final BooleanProperty lost;

    private static final Image headImg;
    private static final Image wagonImg;
    private static final Image turnImg;

    static {
        headImg = new Image(Player.class.getResource("head.png").toExternalForm());
        wagonImg = new Image(Player.class.getResource("wagon.png").toExternalForm());
        turnImg = new Image(Player.class.getResource("turn.png").toExternalForm());
    }

    public Player() {
        super();
        lastDirectionInput = null;
        lost = new SimpleBooleanProperty(false);
        position = new SimpleObjectProperty<>(new Position(0, 0));
        width = new SimpleDoubleProperty(0);
        height = new SimpleDoubleProperty(0);
        head = new PlayerPart(0, 0, Color.BLACK, position.get().posX, position.get().posY);
        this.getChildren().add(head);
        initHeadTouchedTail();

        position.addListener((observable, oldValue, newValue) -> onPosChanged());
    }

    private void onPosChanged() {
        //On supprime le dernier bout
        getChildren().remove(getChildren().size() - 1);
        getChildren().forEach(c -> ((Rectangle) c).setFill(new ImagePattern(wagonImg)));
        //On remet une nouvelle tête
        head = new PlayerPart(0, 0, Color.BLACK, position.get().posX, position.get().posY);
        head.setFill(new ImagePattern(headImg));

        //Changement de la direction du carré en fonction de la direction du serpent
        if (lastDirectionInput != null) {
            switch (lastDirectionInput) {
                case DROITE:
                    head.setRotate(90);
                    break;
                case GAUCHE:
                    head.setRotate(-90);
                    break;
                case BAS:
                    head.setRotate(180);
                    break;
            }
        }
        initRect(head);
    }

    /**
     * Fait grandir la queue de 1
     */
    public void addTailSize() {
        PlayerPart r = new PlayerPart(0, 0, Color.GREEN, position.get().posX, position.get().posY);
        initRect(r);
    }

    /**
     * Initialise un morceau du joueur.
     * Bind les propriétés
     * @param r Le morceau a init
     */
    private void initRect(PlayerPart r) {
        r.widthProperty().bind(width);
        r.heightProperty().bind(height);
        r.xProperty().bind(width.multiply(r.posX));
        r.yProperty().bind(height.multiply(r.posY));
        getChildren().add(0, r);
    }

    /**
     * Récupère tous les carrés du joueur et retourne toues les position des carré qui formes le joueur
     * @return Une liste de position (x,y)
     */
    public Collection<Pair<Integer, Integer>> getAllPosition() {
        return getChildren().stream().map(c -> {
            PlayerPart part = (PlayerPart) c;
            return new Pair<>(part.posX, part.posY);
        }).collect(Collectors.toList());
    }

    /**
     * Bind la tete du snake au corps pour vérifié si la tete a foncé dans le corps
     */
    private void initHeadTouchedTail() {
        position.addListener((observable, oldValue, newValue) -> {
            if (getAllPosition().contains(new Pair<>(newValue.posX, newValue.posY))) {
                lastDirectionInput = null;
                lost.setValue(true);
            }
        });
    }

    public void setPosX(int x) {
        position.set(new Position(x, position.get().posY));
    }

    public void setPosY(int y) {
        position.set(new Position(position.get().posX, y));
    }

    /**
     * Class poru encapsuler un morceau de joueur
     */
    static class PlayerPart extends Rectangle {
        final int posX;
        final int posY;

        public PlayerPart(double width, double height, Paint fill, int posX, int posY) {
            super(width, height, fill);
            this.posX = posX;
            this.posY = posY;
        }
    }

    /**
     * Class pour encapsuler une position
     */
    static class Position {
        final int posX;
        final int posY;

        public Position(int posX, int posY) {
            this.posX = posX;
            this.posY = posY;
        }
    }
}
