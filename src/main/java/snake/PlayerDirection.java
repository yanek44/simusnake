package snake;

/**
 * Represente les direction possible prise par le joueur
 */
public enum PlayerDirection {
    DROITE,GAUCHE,HAUT,BAS
}
